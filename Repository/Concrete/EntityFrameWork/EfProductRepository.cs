﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository.Abstract;
using Repository.Entities;

namespace Repository.Concrete.EntityFrameWork
{
    public class EfProductRepository : EfGenericRepository<Product>, IProductRepository
    {
        public EfProductRepository(MultipleClassDependencyInjectContext context) : base(context)
        {
        }

        public MultipleClassDependencyInjectContext EduraContext
        {
            get { return context as MultipleClassDependencyInjectContext; }
        }

        public List<Product> GetTop5Products()
        {
            return EduraContext.Products
                .OrderByDescending(i => i.ProductId)
                .Take(5)
                .ToList();
        }
    }
}
