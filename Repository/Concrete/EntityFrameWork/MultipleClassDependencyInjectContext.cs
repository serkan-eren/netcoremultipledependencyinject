﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Repository.Entities;

namespace Repository.Concrete.EntityFrameWork
{
    public class MultipleClassDependencyInjectContext : DbContext
    {
        public MultipleClassDependencyInjectContext(DbContextOptions<MultipleClassDependencyInjectContext> options) :
            base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductCategory>()
                .HasKey(pk => new { pk.ProductId, pk.CategoryId });
        }
    }
}

