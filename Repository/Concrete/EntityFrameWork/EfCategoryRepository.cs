﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repository.Abstract;
using Repository.Entities;

namespace Repository.Concrete.EntityFrameWork
{
   public class EfCategoryRepository: EfGenericRepository<Category>, ICategoryRepository
    {
        public EfCategoryRepository(MultipleClassDependencyInjectContext context) : base(context)
        {
        }

        public MultipleClassDependencyInjectContext EduraContext
        {
            get { return context as MultipleClassDependencyInjectContext; }
        }

        public Category GetByName(string name)
        {
            return EduraContext.Categories
                .Where(i => i.CategoryName == name)
                .FirstOrDefault();
        }
    }
}
