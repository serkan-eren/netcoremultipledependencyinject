﻿using System;
using System.Collections.Generic;
using System.Text;
using Repository.Abstract;

namespace Repository.Concrete.EntityFrameWork
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private readonly MultipleClassDependencyInjectContext dbContext;

        public EfUnitOfWork(MultipleClassDependencyInjectContext _dbContext)
        {
            dbContext = _dbContext ?? throw new ArgumentNullException("dbcontext can not be null");
        }

        private IProductRepository _products;
        private ICategoryRepository _categories;


        public IProductRepository Products
        {
            get
            {
                return _products ?? (_products = new EfProductRepository(dbContext));
            }
        }

        public ICategoryRepository Cegories
        {
            get
            {
                return _categories ?? (_categories = new EfCategoryRepository(dbContext));
            }
        }

        public int SaveChanges()
        {
            try
            {
                return dbContext.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }

        }

        public void Dispose()
        {
            dbContext.Dispose();
        }


    }
}
