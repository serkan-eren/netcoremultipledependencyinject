﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository Products { get; }
        ICategoryRepository Cegories { get; }

        int SaveChanges();
    }
}
