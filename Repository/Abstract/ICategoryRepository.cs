﻿using System;
using System.Collections.Generic;
using System.Text;
using Repository.Entities;

namespace Repository.Abstract
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
        Category GetByName(string name);
    }
}
