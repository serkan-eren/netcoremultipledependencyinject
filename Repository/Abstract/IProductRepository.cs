﻿using System;
using System.Collections.Generic;
using System.Text;
using Repository.Entities;

namespace Repository.Abstract
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        List<Product> GetTop5Products();
    }
}
